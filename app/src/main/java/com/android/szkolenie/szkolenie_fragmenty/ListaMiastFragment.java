package com.android.szkolenie.szkolenie_fragmenty;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by papcio28 on 07.05.2015.
 */
public class ListaMiastFragment extends Fragment {
    // NIE ROBIMY KONSTRUKTORA !!

    private ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Ladujemy widoki z pliku XML
        View mView = inflater.inflate(R.layout.fragment_lista_miast, container, false);

        mListView = (ListView) mView.findViewById(R.id.lista_miast);

        return mView;
    }

    /**
     * Wykonywana bezposrednio po metodzie onCreateView
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState); // To zawsze musi być ! Nie mozna tego wyrzucac !
        mListView = (ListView) view.findViewById(R.id.lista_miast);

        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getListaMiast());
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // Pobieramy nazwe miasta z adaptera
                String mMiasto = (String) adapterView.getAdapter().getItem(position);

                // Otwarcie szczegolow miasta
                MainActivity mActivity = (MainActivity) getActivity();
                mActivity.otworzSzczegolyMiasta(mMiasto);
            }
        });
    }

    private String[] getListaMiast() {
        return new String[] {"Warszawa", "Wrocław", "Poznań", "Katowice"};
    }
}
