package com.android.szkolenie.szkolenie_fragmenty;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListaMiastFragment mListaFragment = new ListaMiastFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content, mListaFragment, "lista").commit();
    }

    public void otworzSzczegolyMiasta(String mNazwa) {
        SzczegolyMiastaFragment mFragment = new SzczegolyMiastaFragment();
        Bundle mArguments = new Bundle();

        mArguments.putString(SzczegolyMiastaFragment.MIASTO_ARG, mNazwa);
        mFragment.setArguments(mArguments);

        // Otwarcie fragmentu szczegolow
        getSupportFragmentManager().
                beginTransaction().
                add(R.id.content, mFragment, "details").
                addToBackStack(null).
                commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
