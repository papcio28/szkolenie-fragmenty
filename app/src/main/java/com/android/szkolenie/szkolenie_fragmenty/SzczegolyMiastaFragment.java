package com.android.szkolenie.szkolenie_fragmenty;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by papcio28 on 07.05.2015.
 */
public class SzczegolyMiastaFragment extends Fragment {

    public static final String MIASTO_ARG = "miasto.nazwa";

    private EditText mMiastoNazwa;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_szczegoly_miasta, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMiastoNazwa = (EditText) view.findViewById(R.id.miasto_nazwa_label);

        // Przypisujemy nazwe miasta
        String mMiasto = getArguments().getString(MIASTO_ARG);

        mMiastoNazwa.setText(mMiasto);
    }
}
